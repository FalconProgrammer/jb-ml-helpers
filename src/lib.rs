#[allow(dead_code)]

#[macro_use]
extern crate log;

pub mod mean_shift_clustering;
pub mod ndarray_helpers;

pub mod torch_mods;

pub mod torch_models;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}

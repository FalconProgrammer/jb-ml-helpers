/*
pub struct MeanShiftClusterer {
	bandwidth: f64,
	seeds: Option<Matrix<f64>>,
	min_bin_freq: i64,
	cluster_all: bool,
	max_iter: usize,
	clusters: Option<Matrix<f64>>,
}
*/


use ndarray::{s, Array1, Array2, ArrayView1, ArrayView2, Axis};
use ndarray_linalg::norm::Norm;



pub struct MeanShiftCluster{
	clusters: Array2<f64>,
}

pub enum Kernel {
	NoKernel,
	GaussianKernel,
}



impl MeanShiftCluster {
	pub fn get_clusters(&self) -> ArrayView2<f64> {
		self.clusters.view()
	}

	pub fn estimate_bandwidth(inputs: ArrayView2<f64>, quantile: f64) -> f64
	{
		let num_neighbours_f = inputs.nrows() as f64 * quantile;
		let num_neighbours = if num_neighbours_f < 1. {
			1 as usize
		} else {
			num_neighbours_f as usize
		};

		let distances = get_row_distances(inputs.clone());

		let bandwidth : f64 = distances
			.iter()
			.map(|d| {
				let mut sorted_d: Vec<_> = d.iter().cloned().collect();
				sorted_d.sort_by(|a, b| a.partial_cmp(b).unwrap());

				// Get Maximum distance at quantile
				sorted_d[num_neighbours - 1]
			}).sum();

		let bandwidth = bandwidth / inputs.nrows() as f64;

		bandwidth
	}

	pub fn train(inputs: ArrayView2<f64>) -> Result<MeanShiftCluster, &'static str> {
		let bandwidth = Self::estimate_bandwidth(inputs.clone(), 0.3);

		return Self::train_params(inputs, bandwidth, 300, false, Kernel::NoKernel, None);
	}

	pub fn train_params(
		inputs: ArrayView2<f64>,
		bandwidth: f64,
		max_iter: usize,
		adaptive: bool,
		kernel_type: Kernel,
		initial_seeds: Option<ArrayView2<f64>>,
	) -> Result<MeanShiftCluster, &'static str>	{
		// If we don't have seeds, copy the inputs
		let mut means = match initial_seeds {
			Some(seed) => seed.to_owned(),
			None => inputs.to_owned(),
		};

		// Useful Variables
		let _n_samples = inputs.nrows();
		let feature_size = inputs.ncols();

		// Defines the threshold that will say when mean has converged
		let stop_thresh = 1e-3 * bandwidth;

		// Used to store how many neighbours each mean has within its bandwidth
		let mut num_neighbours = vec![0 as usize; means.nrows() as usize];

		use rayon::iter::*;

		// Calculate bandwidths for adaptive
		let local_bandwidths: Vec<_> = if adaptive {
			means
				.axis_iter(Axis(0))
				.map(|current_mean| {
					let nearest_neighbour = means
						.axis_iter(Axis(0))
						.filter_map(|other_mean| {
							let dist = euclidean_distance(&current_mean, &other_mean, false);

							if dist == 0.0 {
								None
							} else {
								Some((other_mean, dist))
							}
						})
						.min_by(|(_, a), (_, b)| a.partial_cmp(b).unwrap())
						.unwrap();

					(&current_mean - &nearest_neighbour.0).norm_l1()
				})
				.collect()
		} else {
			Vec::new()
		};

		means
			.axis_iter_mut(Axis(0))
			.zip(num_neighbours.iter_mut())
			.enumerate()
			.par_bridge()
			.for_each(|(i, (mut current_mean, num_neighbours))| {
				let mut final_count: usize = 0;

				// Perform up to self.max_iter iterations
				for _iter in 0..max_iter {
					// Store the old mean (Should move current_mean_mat into old)
					let old: Array1<f64> = current_mean.to_owned();

					// Gather the points within the bandwidth distance
					let points_within = nearest_points(current_mean.view(), inputs.view(), bandwidth);

					// Store the number of neighbours for post processing
					*num_neighbours = points_within.len();

					match &kernel_type {
						Kernel::NoKernel => {
							// If we have no points nearby, then we can't do anything so break
							if points_within.is_empty() {
								break;
							}

							// Calculate new mean as mean of points within bandwidth
							current_mean.fill(0.);

							points_within
								.iter()
								.map(|point_pos| inputs.row(*point_pos))
								.for_each(|slice| {
									current_mean += &slice;
								});

							current_mean /= points_within.len() as f64;
						}
						kernel => {
							// If we're using adaptive version, grab the local bandwidth
							let bandwidth = if adaptive { local_bandwidths[i] } else { bandwidth };

							// Calculate all the weights
							let weights: Vec<f64> = points_within
								.iter()
								.map(|point_pos| inputs.row(*point_pos))
								.map(|point| {
									let distance = euclidean_distance(&current_mean.view(), &point, false);

									match &kernel {
										Kernel::GaussianKernel => {
											let g_part1 = 1. / (bandwidth * (2. * std::f64::consts::PI).sqrt());
											let g_part2 = (-0.5 * (distance / bandwidth).powf(2.)).exp();

											return g_part1 * g_part2;
										}
										_ => panic!("Unimplemented Shifting Method"),
									};
								})
								.collect();

							// Calculate new mean as weighted mean of points within bandwidth
							current_mean.fill(0.);

							let scale_factor: f64 = points_within
								.iter()
								.map(|point_pos| inputs.row(*point_pos))
								.zip(weights.iter())
								.map(|(point, weight)| {
									current_mean += &(&point * *weight);

									weight
								})
								.sum();

							current_mean /= scale_factor;
						}
					};

					// If we haven't shifted more than the stop threshold, then break
					let l2_diff: f64 = (&current_mean - &old).norm_l2();

					if l2_diff < stop_thresh {
						break;
					}

					final_count += 1;
				}

				debug!(
					"Finished seed {} at iteration {} with {} points in range",
					i, final_count, num_neighbours
				);
			});

		// Sort by How many neighbours
		let mut tupled_res: Vec<_> = means.axis_iter(Axis(0)).zip(num_neighbours.iter()).collect();
		tupled_res.sort_by(|(_, neighbour_a), (_, neighbour_b)| neighbour_a.partial_cmp(neighbour_b).unwrap());

		let sorted_tuple_array = Array2::from_shape_vec(
			(tupled_res.len(), feature_size),
			tupled_res.iter().map(|(a, _)| a).flatten().cloned().collect(),
		)
		.unwrap();

		// Determine which points to remove
		let mut unique = vec![true; tupled_res.len()];

		tupled_res.iter().enumerate().for_each(|(i, (mean, _))| {
			if unique[i] {
				let comp = sorted_tuple_array.slice(s![i + 1.., ..]);

				let neighbours = nearest_points(*mean, comp.view(), bandwidth);

				neighbours.iter().for_each(|j| {
					unique[*j + i + 1] = false;
				});

				unique[i] = true;
			}
		});

		let unique_count = unique.iter().filter(|u| **u).count();

		// Extract the final Clusters
		let cluster_data: Vec<_> = tupled_res
			.iter()
			.zip(unique)
			.filter_map(|((row, _), unique)| if unique { Some(row) } else { None })
			.flatten()
			.cloned()
			.collect();

		let clusters = Array2::from_shape_vec((unique_count, feature_size), cluster_data).unwrap();

		debug!("{} rows and {} cols", clusters.nrows(), clusters.ncols());

		Ok(MeanShiftCluster { clusters: clusters })
	}

	pub fn predict(&self, input: ArrayView1<f64>) -> usize {
		self.clusters
			.axis_iter(Axis(0))
			.enumerate()
			.map(|(cluster_id, cluster)| (cluster_id, euclidean_distance(&input, &cluster, false)))
			.min_by(|(_, a), (_, b)| a.partial_cmp(b).unwrap())
			.unwrap()
			.0
	}
}

fn get_row_distances<T: num::traits::Float>(input: ArrayView2<T>) -> Vec<Vec<T>> {
	let mut distances: Vec<Vec<T>> = vec![vec![T::zero(); input.nrows()]; input.nrows()];

	for i in 0..input.nrows() {
		for j in 0..input.nrows() {
			if i == j {
				break;
			}
			if distances[i][j] != T::zero() {
				break;
			}

			distances[i][j] = euclidean_distance(&input.row(i), &input.row(j), true);
			distances[j][i] = distances[i][j];
		}
	}

	return distances;
}

fn euclidean_distance<T: num::traits::Float>(a: &ArrayView1<T>, b: &ArrayView1<T>, root: bool) -> T {
	let sum = ((a - b).mapv(|a| a * a)).sum();

	if root {
		sum.sqrt()
	} else {
		sum
	}
}

fn nearest_points<T: num::traits::Float>(point: ArrayView1<T>, comp_points: ArrayView2<T>, limit: T) -> Vec<usize> {
	comp_points
		.axis_iter(Axis(0))
		.enumerate()
		.filter_map(|(i, c_p)| {
			let eucl_dist = euclidean_distance(&c_p, &point, true);

			if eucl_dist <= limit {
				Some(i)
			} else {
				None
			}
		})
		.collect()
}

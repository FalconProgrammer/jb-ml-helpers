pub fn tensor_to_array<T : std::convert::From<tch::Tensor> + Clone + Copy>(tensor: &tch::Tensor) -> Result<ndarray::Array2<T>, String>
	where std::vec::Vec<T>: std::convert::From<tch::Tensor>
{
	// NOTE (JB) Check input dimensions
	if tensor.dim() != 2 {
		return Err(format!(
			"Number of dimensions in input tensor is incorrect. is {} but has to be 2",
			tensor.dim()
		));
	}

	// NOTE (JB) Get the size of the matrix
	let (dim1, dim2) = tensor.size2().unwrap();

	// NOTE (JB) Get all the values from the Tensor (TODO: Can this be done via iterators?)
	let mut all_outputs = Vec::new();

	for i in 0..dim1 {
		let prediction = Vec::<T>::from(tensor.get(i));

		all_outputs.push(prediction);
	}

	// NOTE (JB) Flatten the vector for the input to the matrix
	let flattened: Vec<T> = all_outputs.iter().flat_map(|array| array.iter()).map(|v| *v).collect();

	let arr = ndarray::Array2::from_shape_vec((dim1 as usize, dim2 as usize), flattened)
		.unwrap()
		.to_owned();

	Ok(arr)
}

pub fn array_to_tensor_f32(array: ndarray::ArrayView2<f32>) -> tch::Tensor {
	// NOTE (JB) Convert the data into a tensor
	let t : tch::Tensor = tch::Tensor::of_slice(array.as_slice().unwrap());

	// NOTE (JB) Reshape to desired dimensions
	t.reshape(
		array
			.shape()
			.iter()
			.cloned()
			.map(|a| a as i64)
			.collect::<Vec<_>>()
			.as_slice(),
	)
}

pub fn array_to_tensor_f64(array: ndarray::ArrayView2<f64>) -> tch::Tensor {
	// NOTE (JB) Convert the data into a tensor
	let t : tch::Tensor = tch::Tensor::of_slice(array.as_slice().unwrap());

	// NOTE (JB) Reshape to desired dimensions
	t.reshape(
		array
			.shape()
			.iter()
			.cloned()
			.map(|a| a as i64)
			.collect::<Vec<_>>()
			.as_slice(),
	)
}

pub fn vec_axis_to_array<T : Clone>(vec_to_convert: &Vec<ndarray::ArrayView1<T>>, use_as_row: bool) -> ndarray::Array2<T> {
	let (rows, cols) = if use_as_row {
		(vec_to_convert.len(), vec_to_convert[0].len())
	} else {
		(vec_to_convert[0].len(), vec_to_convert.len())
	};

	let flattened: Vec<T> = vec_to_convert.iter().flatten().cloned().collect();

	ndarray::Array2::from_shape_vec((rows, cols), flattened).unwrap()
}

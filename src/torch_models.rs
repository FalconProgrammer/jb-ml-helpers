pub fn create_autoencoder(
    input_feature_size: &i64,
    layer_scales: &[f64],
    var_store: &tch::nn::Path,
) -> crate::torch_mods::Sequential {
    let mut model = crate::torch_mods::seq();
    let mut input_size = *input_feature_size;

    let sizes: Vec<_> = layer_scales
        .iter()
        .map(|scale| (scale * (*input_feature_size as f64)) as i64)
        .enumerate()
        .map(|(i, output_size)| {
            model.add(tch::nn::linear(
                var_store / format!("layer_{}", i + 1),
                input_size,
                output_size,
                Default::default(),
            ));
            model.add_fn(|xs| xs.sigmoid());

            input_size = output_size;

            output_size
        })
        .collect();

    model.add(tch::nn::linear(
        var_store / "layer_final",
        input_size,
        *input_feature_size,
        Default::default(),
    ));;
    model.add_fn(|xs| xs.sigmoid());

    model
}